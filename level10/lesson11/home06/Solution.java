package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

import java.util.Date;

public class Solution {
	public static void main(String[] args) {

	}

	public static class Human {
		//напишите тут ваши переменные и конструкторы
		String firstName;
		String secondName;
		boolean sex;
		byte age;
		Date birthday;
		String eyeColor;

		public Human(String secondName) {
			this.secondName = secondName;
		}

		public Human(String firstName, String secondName) {
			this.firstName = firstName;
			this.secondName = secondName;
		}

		public Human(String firstName, String secondName, boolean sex) {
			this.firstName = firstName;
			this.secondName = secondName;
			this.sex = sex;
		}

		public Human(String secondName, boolean sex, byte age) {
			this.secondName = secondName;
			this.sex = sex;
			this.age = age;
		}

		public Human(String firstName, String secondName, boolean sex, byte age) {
			this.firstName = firstName;
			this.secondName = secondName;
			this.sex = sex;
			this.age = age;
		}

		public Human(String firstName, String secondName, boolean sex, Date birthday) {
			this.firstName = firstName;
			this.secondName = secondName;
			this.sex = sex;
			this.birthday = birthday;
		}

		public Human(String firstName, String secondName, boolean sex, byte age, String eyeColor) {
			this.firstName = firstName;
			this.secondName = secondName;
			this.sex = sex;
			this.age = age;
			this.eyeColor = eyeColor;
		}

		public Human(String firstName, String secondName, boolean sex, Date birthday, String eyeColor) {
			this.firstName = firstName;
			this.secondName = secondName;
			this.sex = sex;
			this.birthday = birthday;
			this.eyeColor = eyeColor;
		}

		public Human(String secondName, boolean sex, byte age, String eyeColor) {
			this.secondName = secondName;
			this.sex = sex;
			this.age = age;
			this.eyeColor = eyeColor;
		}

		public Human(String firstName, String secondName, boolean sex, byte age, Date birthday, String eyeColor) {
			this.firstName = firstName;
			this.secondName = secondName;
			this.sex = sex;
			this.age = age;
			this.birthday = birthday;
			this.eyeColor = eyeColor;
		}
	}
}
