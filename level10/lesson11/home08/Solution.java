package com.javarush.test.level10.lesson11.home08;

import java.util.ArrayList;

/* Массив списков строк
Создать массив, элементами которого будут списки строк. Заполнить массив любыми данными и вывести их на экран.
*/

public class Solution {
	public static void main(String[] args) {
		ArrayList<String>[] arrayOfStringList = createList();
		printList(arrayOfStringList);
	}

	public static ArrayList<String>[] createList() {
		//напишите тут ваш код
		final byte LISTLENGTH = 8;
		ArrayList<String> list[] = new ArrayList[LISTLENGTH];

//		System.out.println(list.length);

		for(int i = 0; i < list.length; i++) {
			list[i] = new ArrayList<String>();
			for(int k = 0; k < 4; k++)
				list[i].add(Integer.toString(k));
		}

		return list;
	}

	public static void printList(ArrayList<String>[] arrayOfStringList) {
		for(ArrayList<String> list : arrayOfStringList) {
			for(String s : list) {
				System.out.println(s);
			}
		}
	}
}