package com.javarush.test.level12.lesson04.task05;

/* Три метода возвращают максимальное из двух переданных в него чисел
Написать public static методы: int max(int, int), long max (long, long), double max (double, double).
Каждый метод должен возвращать максимальное из двух переданных в него чисел.
*/

public class Solution {
	public static void main(String[] args) {

	}

	//Напишите тут ваши методы
	public static int max(int i, int k) {
		int max = (i > k) ? i : k;
		return max;
	}
	public static long max(long i, long k) {
		long max = (i > k) ? i : k;
		return max;
	}
	public static double max(double i, double k) {
		double max = (i > k) ? i : k;
		return max;
	}
}
