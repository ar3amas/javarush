package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human).
Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

import java.util.ArrayList;

public class Solution {
	public static void main(String[] args) {
		//напишите тут ваш код

		ArrayList<Human> family = new ArrayList<Human>();

		family.add(new Human("Ваня", true, 72, null, null));
		family.add(new Human("Вася", true, 70, null, null));
		family.add(new Human("Маня", false, 68, null, null));
		family.add(new Human("Маша", false, 66, null, null));

		family.add(new Human("Саша", true, 38, family.get(0), family.get(2)));
		family.add(new Human("Вика", false, 36, family.get(1), family.get(3)));

		family.add(new Human("Надя", false, 12, family.get(4), family.get(5)));
		family.add(new Human("Рита", false, 9, family.get(4), family.get(5)));
		family.add(new Human("Артур", true, 6, family.get(4), family.get(5)));

		for(Human h : family)
			System.out.println(h);

/*
		Human grandpa1 = new Human("Ваня", true, 72, null, null);
		Human grandpa2 = new Human("Вася", true, 70, null, null);
		Human grandma1 = new Human("Маня", false, 68, null, null);
		Human grandma2 = new Human("Маша", false, 66, null, null);

		Human father = new Human("Саша", true, 38, grandpa1, grandma1);
		Human mother = new Human("Вика", false, 36, grandpa2, grandma2);

		Human child1 = new Human("Надя", false, 12, father, mother);
		Human child2 = new Human("Рита", false, 9, father, mother);
		Human child3 = new Human("Артур", true, 6, father, mother);
*/

	}

	public static class Human {
		//напишите тут ваш код
		String name;
		Boolean sex;
		Integer age;
		Human father;
		Human mother;

		public Human(String name, Boolean sex, Integer age, Human father, Human mother) {
			this.name = name;
			this.sex = sex;
			this.age = age;
			this.father = father;
			this.mother = mother;
		}

		public String toString() {
			String text = "";
			text += "Имя: " + this.name;
			text += ", пол: " + (this.sex ? "мужской" : "женский");
			text += ", возраст: " + this.age;

			if(this.father != null)
				text += ", отец: " + this.father.name;

			if(this.mother != null)
				text += ", мать: " + this.mother.name;

			return text;
		}
	}

}
