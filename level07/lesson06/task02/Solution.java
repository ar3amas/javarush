package com.javarush.test.level07.lesson06.task02;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* Самая длинная строка
1. Создай список строк.
2. Считай с клавиатуры 5 строк и добавь в список.
3. Используя цикл, найди самую длинную строку в списке.
4. Выведи найденную строку на экран.
5. Если таких строк несколько, выведи каждую с новой строки.
*/
public class Solution {
	public static void main(String[] args) throws Exception {
		ArrayList<String> texts = new ArrayList<String>();

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		for(int i = 0; i < 5; i++)
			texts.add(reader.readLine());

		int maxLength = 0;

		for(int l = 0; l < (texts.size() - 1); l++) {
			if(texts.get(l).length() < (texts.get(l + 1).length()))
				maxLength = texts.get(l + 1).length();
		}

		for(String s : texts) {
			if(s.length() == maxLength)
				System.out.println(s);
		}
	}
}

