package com.javarush.test.level07.lesson04.task05;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/* Один большой массив и два маленьких
1. Создать массив на 20 чисел.
2. Ввести в него значения с клавиатуры.
3. Создать два массива на 10 чисел каждый.
4. Скопировать большой массив в два маленьких: половину чисел в первый маленький, вторую половину во второй маленький.
5. Вывести второй маленький массив на экран, каждое значение выводить с новой строки.
*/

public class Solution {
	public static void main(String[] args) throws Exception {
		int[] bigList = new int[20];
		int[] smallListFirst = new int[10];
		int[] smallListSecond = new int[10];

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		for(int i = 0; i < bigList.length; i++)
			bigList[i] = Integer.parseInt(reader.readLine());

		smallListFirst = Arrays.copyOf(bigList, (bigList.length / 2 - 1));
		smallListSecond = Arrays.copyOfRange(bigList, (bigList.length / 2), (bigList.length));

		for(int i: smallListSecond)
			System.out.println(i);
	}
}
