package com.javarush.test.level07.lesson09.task03;

import java.util.ArrayList;

/* Слово «именно»
1. Создай список из слов «мама», «мыла», «раму».
2. После каждого слова вставь в список строку, содержащую слово «именно».
3. Используя цикл for вывести результат на экран, каждый элемент списка с новой строки.
*/

public class Solution {
	public static void main(String[] args) throws Exception {
		ArrayList<String> namely = new ArrayList<String>();

		namely.add("мама");
		namely.add("мыла");
		namely.add("раму");

		for(int i = 0; i < namely.size(); i++)
			namely.add(++i, "именно");

		for(String s : namely)
			System.out.println(s);

	}
}
