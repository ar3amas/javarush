package com.javarush.test.level16.lesson13.bonus02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* Клубок
1. Создай 5 различных своих нитей c отличным от Thread типом:
1.1. нить 1 должна бесконечно выполняться;
1.2. нить 2 должна выводить "InterruptedException" при возникновении исключения InterruptedException;
1.3. нить 3 должна каждые полсекунды выводить "Ура";
1.4. нить 4 должна реализовать интерфейс Message, при вызове метода showWarning нить должна останавливаться;
1.5. нить 5 должна читать с консоли цифры пока не введено слово "N", а потом вывести в консоль сумму введенных цифр.
2. В статическом блоке добавь свои нити в List<Thread> threads в перечисленном порядке.
3. Нити не должны стартовать автоматически.
Подсказка: Нить 4 можно проверить методом isAlive()
*/


public class Solution {
    public static List<Thread> threads = new ArrayList<>(5);

    static {
        threads.add(new Thread1());
        threads.add(new Thread2());
        threads.add(new Thread3());
        threads.add(new Thread4());
        threads.add(new Thread5());
    }

    /*
    // Проверкака
    public static void main(String[] args) throws Exception {

        // Проверка Thread1
        Thread t1 = threads.get(0);
        t1.start();
        System.out.println("Thread1, ты ещё живой? " + (t1.isAlive() ? "+да" : "-нет"));

        // Проверка Thread2
        Thread t2 = threads.get(1);
        t2.start();
        System.out.print("Thread2, лови прерывание! -");
        t2.interrupt();
        t2.join();

        // Проверка Thread3
        Thread t3 = threads.get(2);
        System.out.println("Thread3, скажи чего?!");
        t3.start();
        t3.sleep(1500);
        t3.interrupt();
        t3.join();

        // Проверка Thread4
        Thread t4 = threads.get(3);
        t4.start();
        System.out.print("Thread4, держи месседж и тормози");
        ((Message) t4).showWarning();
        System.out.println(", хорошо? " + (t4.isAlive() ? "-Никак нет" : "+Так точно!"));

        // Проверка Thread5
        Thread t5 = threads.get(4);
        t5.start();
        System.out.println("Thread5, суммируй вводимые числа, и покажи результат после \"N\", пожалуйста.");
    }
    */

    public static class Thread1 extends Thread {

        @Override
        public void run() {
            while(true) ;
        }
    }

    public static class Thread2 extends Thread {

        @Override
        public void run() {
            try {
                while(!isInterrupted()) ;
                throw new InterruptedException();
            }
            catch(InterruptedException e) {
                System.out.println("InterruptedException");
            }
        }
    }

    public static class Thread3 extends Thread {

        @Override
        public void run() {
            try {
                while(!isInterrupted()) {
                    System.out.println("Ура");
                    sleep(500);
                }
            }
            catch(InterruptedException e) {
            }
        }
    }

    public static class Thread4 extends Thread implements Message {

        @Override
        public void showWarning() {

            interrupt();
            try {
                join();
            }
            catch(InterruptedException e) {
            }
        }

        @Override
        public void run() {
            while(!isInterrupted()) ;
        }
    }

    public static class Thread5 extends Thread {

        @Override
        public void run() {
            String s;
            int sum = 0;
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            try {
                while(!(s = reader.readLine()).equals("N")) {
                    sum += Integer.parseInt(s);
                }
                System.out.println(sum);
            }
            catch(IOException e) {
            }
        }
    }
}

