package com.javarush.test.level14.lesson08.home01;

/**
 * Created by k1s on 10.01.16.
 */
public class SuspensionBridge implements Bridge {
	@Override
	public int getCarsCount() {
		return 314;
	}
}
