package com.javarush.test.level14.lesson08.home01;

/**
 * Created by k1s on 10.01.16.
 */
public interface Bridge {
	int getCarsCount();
}
