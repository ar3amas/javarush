package com.javarush.test.level14.lesson06.home01;

/**
 * Created by k1s on 08.01.16.
 */
public abstract class Hen {

	public abstract int getCountOfEggsPerMonth();

	public String getDescription() {
		return "Я курица.";
	}
}
