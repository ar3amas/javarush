package com.javarush.test.level14.lesson06.home01;

/**
 * Created by k1s on 08.01.16.
 */
public class RussianHen extends Hen {
	private static int countOfEggsPerMonth = 100;

	@Override
	public int getCountOfEggsPerMonth() {
		return countOfEggsPerMonth;
	}

	@Override
	public String getDescription() {
		return super.getDescription() + " Моя страна - " + Country.RUSSIA + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.";
	}
}
