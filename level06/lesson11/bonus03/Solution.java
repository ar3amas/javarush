package com.javarush.test.level06.lesson11.bonus03;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

/* Задача по алгоритмам
Задача: Написать программу, которая вводит с клавиатуры 5 чисел и выводит их в возрастающем порядке.
Пример ввода:
3
2
15
6
17
Пример вывода:
2
3
6
15
17
*/

public class Solution {
	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		Vector<Integer> numbers= new Vector<Integer>();
		for(int i= 0; i< 5; i++) {
			numbers.add(Integer.parseInt(reader.readLine()));
		}

		Collections.sort(numbers);

		for(int v : numbers){
			System.out.println(v);
		}
	}
}
