package com.javarush.test.level05.lesson12.home02;

/* Man and Woman
1. Внутри класса Solution создай public static классы Man и Woman.
2. У классов должны быть поля: name(String), age(int), address(String).
3. Создай конструкторы, в которые передаются все возможные параметры.
4. Создай по два объекта каждого класса со всеми данными используя конструктор.
5. Объекты выведи на экран в таком формате [name + " " + age + " " + address].
*/

public class Solution {
	public static void main(String[] args) {
		//создай по два объекта каждого класса тут
		Man man1= new Man("Саша", 33, "Мск");
		Man man2= new Man("Артур", 10, "Спб");

		Woman wom1= new Woman("Надя", 35, "Смр");
		Woman wom2= new Woman("Вика", 1, "Дрф");

		//выведи их на экран тут
		System.out.println(man1.toString());
		System.out.println(man2.toString());

		System.out.println(wom1.toString());
		System.out.println(wom2.toString());

	}

	//добавьте тут ваши классы

	public static class Man {
		String name, address;
		int age;

		public Man(String name, int age, String address) {
			this.name = name;
			this.address = address;
			this.age = age;
		}

		public String toString() {
			return name+ " "+ age+ " "+ address;
		}
	}

	public static class Woman {
		String name, address;
		int age;

		public Woman(String name, int age, String address) {
			this.name = name;
			this.address = address;
			this.age = age;
		}

		public String toString() {
			return name+ " "+ age+ " "+ address;
		}
	}
}
