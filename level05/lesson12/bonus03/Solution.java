package com.javarush.test.level05.lesson12.bonus03;

import java.io.*;
import java.util.Collections;
import java.util.Vector;

/* Задача по алгоритмам
Написать программу, которая:
1. вводит с консоли число N > 0
2. потом вводит N чисел с консоли
3. выводит на экран максимальное из введенных N чисел.
*/

public class Solution {
	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		//напишите тут ваш код

		int n;

		do {
			n = Integer.parseInt(reader.readLine());
		} while (n == 0);

		Vector<Integer> nArray = new Vector();

		while (n > 0) {
			nArray.add(Integer.parseInt(reader.readLine()));
			n--;
		}

		Collections.sort(nArray);
		int maximum = nArray.lastElement();

		System.out.println(maximum);
	}
}
