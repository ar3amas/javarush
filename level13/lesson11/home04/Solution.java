package com.javarush.test.level13.lesson11.home04;

/* Запись в файл
1. Прочесть с консоли имя файла.
2. Считывать строки с консоли, пока пользователь не введет строку "exit".
3. Вывести абсолютно все введенные строки в файл, каждую строчку с новой стороки.
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class Solution {
	public static void main(String[] args) throws IOException {

		LinkedList<String> enteredLines = new LinkedList();
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		do {
			enteredLines.add(reader.readLine());
		} while(!enteredLines.get(enteredLines.size() - 1).equals("exit"));

		reader.close();

		for(String s : enteredLines) {
			System.out.println(s);
		}
	}
}
