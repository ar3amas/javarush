package com.javarush.test.level13.lesson11.home03;

/* Чтение файла
1. Считать с консоли имя файла.
2. Вывести в консоль(на экран) содержимое файла.
3. Не забыть освободить ресурсы. Закрыть поток чтения с файла и поток ввода с клавиатуры.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
	public static void main(String[] args) throws IOException {
		//add your code here
		//Чтение введёного имени файла
		BufferedReader readLine = new BufferedReader(new InputStreamReader(System.in));
		String fileName = readLine.readLine();
		readLine.close();

		//Чтение файла
		BufferedReader readFile = new BufferedReader(new FileReader(fileName));
		String readedFileLine;
		while((readedFileLine = readFile.readLine()) != null) {
			System.out.println(readedFileLine);
		}
		readFile.close();
	}
}
