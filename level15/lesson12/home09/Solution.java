package com.javarush.test.level15.lesson12.home09;

/* Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Solution {
	public static void main(String[] args) throws IOException {
		//add your code here
		//считывание строки из консоли  леление на пары
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		URL url = new URL(reader.readLine());
		reader.close();

		//оброботка введёной строки
		int equalSignIdx;
		String objParam = "";

		for(String s : url.getQuery().split("&")) {
			equalSignIdx = s.indexOf('=');
			if(equalSignIdx > 0) {
				System.out.print(s.substring(0, equalSignIdx) + " ");

				if(s.substring(0, equalSignIdx).equals("obj"))
					objParam = s.substring(equalSignIdx + 1);

			} else {
				System.out.print(s + " ");
			}
		}

		if(!objParam.equals("")) {
			System.out.println();
			try {
				alert(Double.parseDouble(objParam));
			} catch(Exception e) {
				alert(objParam);
			}
		}
	}

	public static void alert(double value) {
		System.out.println("double " + value);
	}

	public static void alert(String value) {
		System.out.println("String " + value);
	}
}
