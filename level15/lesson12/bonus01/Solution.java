package com.javarush.test.level15.lesson12.bonus01;

/* Осваивание статического блока
1. В отдельных файлах создать классы Plane и Helicopter, реализующие интерфейс Flyable.
2. Класс Plane должен иметь 1 конструктор с параметром int - количество перевозимых пассажиров.
3. В статическом методе reset класса Solution:
3.1. Считать с консоли параметр типа String. Параметр может быть "plane" или "helicopter".
3.2. Если параметр равен "helicopter", то статическому объекту Flyable result присвоить объект класса Helicopter.
3.3. Если параметр равен "plane", то считать второй параметр типа int, статическому объекту Flyable result присвоить объект класса Plane.
4. В статическом блоке инициализировать Flyable result вызвав метод reset.
5. Закрыть поток ввода методом close().
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
	public static Flyable result;

	static {
		//add your code here - добавьте код тут
		reset();
	}

//	public static void main(String[] args) {
//		System.out.println(result);
//	}

	public static void reset() {
		//add your code here - добавьте код тут
		String readedLine;
		String transportPassengers;

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			readedLine = reader.readLine();

			if(readedLine.equals("helicopter")) {
				result = new Helicopter();

			} else if(readedLine.equals("plane")) {
				result = new Plane(Integer.parseInt(reader.readLine()));

			} else {
				throw new Exception();
			}

			reader.close();

		} catch(Exception e) {
			System.out.println("Ошибка ввода");
			System.out.println(e);

		}
	}
}
