package com.javarush.test.level15.lesson12.bonus01;

/**
 * Created by Alexander on 15.01.2016.
 */
public class Plane implements Flyable {

	private int transportPassengers;

	public Plane(int transportPassengers) {
		this.transportPassengers = transportPassengers;

	}

	@Override
	public void fly() {

	}
}
