package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Alexander on 13.01.2016.
 */
public class Earth implements Planet {
	private static volatile Earth _instance = null;

	private Earth() {}

	public static synchronized Earth getInstance() {
		if (_instance == null)
			synchronized (Earth.class) {
				if (_instance == null)
					_instance = new Earth();
			}
		return _instance;
	}
}
