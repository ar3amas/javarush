package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Alexander on 13.01.2016.
 */
public class Moon implements Planet {
	private static volatile Moon _instance = null;

	private Moon() {}

	public static synchronized Moon getInstance() {
		if (_instance == null)
			synchronized (Moon.class) {
				if (_instance == null)
					_instance = new Moon();
			}
		return _instance;
	}
}
