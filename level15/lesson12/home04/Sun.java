package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Alexander on 13.01.2016.
 */
public class Sun implements Planet {
	private static volatile Sun _instance = null;

	private Sun() {}

	public static synchronized Sun getInstance() {
		if (_instance == null)
			synchronized (Sun.class) {
				if (_instance == null)
					_instance = new Sun();
			}
		return _instance;
	}
}
