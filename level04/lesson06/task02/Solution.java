package com.javarush.test.level04.lesson06.task02;

/* Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int number1= Integer.parseInt(reader.readLine());
            int number2= Integer.parseInt(reader.readLine());
            int number3= Integer.parseInt(reader.readLine());
            int number4= Integer.parseInt(reader.readLine());

            if(number1> number2&& number1> number3&& number1> number4)
                System.out.println(number1);
            else if(number2> number1&& number2> number3&& number2> number4)
                System.out.println(number2);
            else if(number3> number1&& number3> number2&& number3> number4)
                System.out.println(number3);
            else if(number4> number1&& number4> number2&& number4> number3)
                System.out.println(number4);
            else
                System.out.println("Некоторые числа равны!");

        } catch (NumberFormatException e) {
            System.err.println("Неверный формат ввода!");

        }

    }
}
