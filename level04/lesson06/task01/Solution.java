package com.javarush.test.level04.lesson06.task01;

/* Минимум двух чисел
Ввести с клавиатуры два числа, и вывести на экран минимальное из них.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try
        {
            int i1 = Integer.parseInt(reader.readLine());
            int i2 = Integer.parseInt(reader.readLine());

            if (i1 < i2)
                System.out.print(i1);
            else
                System.out.print(i2);
        }
        catch (NumberFormatException e)
        {
            System.err.println("Неверный формат строки!");

        }
    }
}