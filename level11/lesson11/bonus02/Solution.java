package com.javarush.test.level11.lesson11.bonus02;

/* Нужно добавить в программу новую функциональность
Добавь общий базовый класс к классам-фигур:  (фигуры из шахмат).
*/

public class Solution {
	public static void main(String[] args) {
	}

	public class ChessMen {

	}

	public class King extends ChessMen {
	}

	public class Queen extends ChessMen {
	}

	public class Rook extends ChessMen {
	}

	public class Knight extends ChessMen {
	}

	public class Bishop extends ChessMen {
	}

	public class Pawn extends ChessMen {
	}
}
