package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution {
	public static HashMap<String, String> createMap() {
		//напишите тут ваш код
		HashMap<String, String> map = new HashMap<String, String>();
		for(int i = 0; i < 10; i++)
			map.put(Integer.toString(i), Integer.toString(i + 5));

		return map;
	}

	public static int getCountTheSameFirstName(HashMap<String, String> map, String name) {
		//напишите тут ваш код
		int nameCount = 0;
		Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
		while(iterator.hasNext()) {
			if(name.equals(iterator.next().getValue()))
				nameCount++;
		}
		return nameCount;

	}

	public static int getCountTheSameLastName(HashMap<String, String> map, String familiya) {
		//напишите тут ваш код
		int famExist = 0;
		if(map.containsKey(familiya))
			famExist = 1;

		return famExist;
	}
}
