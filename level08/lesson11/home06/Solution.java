package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution {
	public static void main(String[] args) throws Exception {
		//напишите тут ваш код
		Human child1 = new Human("Вася", true, 5, null);
		Human child2 = new Human("Даша", false, 6, null);
		Human child3 = new Human("Ваня", true, 7, null);

		ArrayList<Human> smallchildren = new ArrayList<Human>();
		smallchildren.add(child1);
		smallchildren.add(child2);
		smallchildren.add(child3);

		Human mother = new Human("Вика", false, 32, smallchildren);
		Human father = new Human("Саша", true, 33, smallchildren);

		ArrayList<Human> eltern1 = new ArrayList<Human>();
		eltern1.add(mother);

		ArrayList<Human> eltern2 = new ArrayList<Human>();
		eltern2.add(father);

		Human grandma1 = new Human("Васелиса", false, 56, eltern1);
		Human grandpa1 = new Human("Удмунд", true, 58, eltern1);

		Human grandma2 = new Human("Анжелика", false, 55, eltern2);
		Human grandpa2 = new Human("Банифаций", true, 57, eltern2);

		System.out.println(grandma1);
		System.out.println(grandpa1);

		System.out.println(grandma2);
		System.out.println(grandpa2);

		System.out.println(mother);
		System.out.println(father);

		System.out.println(child1);
		System.out.println(child2);
		System.out.println(child3);
	}

	public static class Human {
		//напишите тут ваш код
		String name;
		Boolean sex;
		int age;
		ArrayList<Human> children;

		public Human(String name, Boolean sex, int age, ArrayList<Human> children) {
			this.name = name;
			this.sex = sex;
			this.age = age;
			this.children = children;
		}

		public String toString() {
			String text = "";
			text += "Имя: " + this.name;
			text += ", пол: " + (this.sex ? "мужской" : "женский");
			text += ", возраст: " + this.age;

			int childCount = 0;
			try {
				childCount = this.children.size();
			} catch(Exception e) {
				childCount = 0;
			}
			if(childCount > 0) {
				text += ", дети: " + this.children.get(0).name;

				for(int i = 1; i < childCount; i++) {
					Human child = this.children.get(i);
					text += ", " + child.name;
				}
			}

			return text;
		}
	}

}
